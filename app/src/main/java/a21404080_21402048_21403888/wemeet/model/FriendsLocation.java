package a21404080_21402048_21403888.wemeet.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FriendsLocation implements Serializable {

    private Double latitude;
    private Double longitude;



    public Double getLatitude() {
        return latitude;
    }
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public FriendsLocation(Double latitude, Double longitude){
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Double getLongitude() {
        return longitude;
    }
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    
}

