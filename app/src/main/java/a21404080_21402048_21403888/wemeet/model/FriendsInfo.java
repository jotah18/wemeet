package a21404080_21402048_21403888.wemeet.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class FriendsInfo implements Serializable {

    //private String photoUrl;
    private String name;
    private List<Room> room;

    public FriendsInfo(String name) {
        this.name = name;

    }

    /*public String getPhotoUrl() {
        return photoUrl;
    }
    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }*/

    /*public FriendsInfo(String name, String photoUrl){
        this.name = name;
        this.photoUrl = photoUrl;
    }*/

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void addGroup(Room group) {
        if(this.room == null) {
            this.room = new ArrayList<>();
        }
        this.room.add(group);
    }
}
