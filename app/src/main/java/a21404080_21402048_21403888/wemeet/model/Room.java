package a21404080_21402048_21403888.wemeet.model;

import java.io.Serializable;
import java.util.List;
//A VARIÁVEL LATNG NÃO É SERIALIZABLE AVISO: NÃO TENTAR

public class Room implements Serializable{
    private Double latitude;
    private Double longitude;
    //private LatLng meetingPlace;
    private List<FriendsInfo> memberList;

    public Room(Double latitude, Double longitude, List<FriendsInfo> memberList) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.memberList = memberList;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public List<FriendsInfo> getMemberList() {
        return memberList;
    }

    public void setMemberList(List<FriendsInfo> memberList) {
        this.memberList = memberList;
    }

    public void addMember(FriendsInfo member) {
        this.memberList.add(member);
    }
}
