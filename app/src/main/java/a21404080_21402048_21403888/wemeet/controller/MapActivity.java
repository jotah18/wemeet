package a21404080_21402048_21403888.wemeet.controller;


import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;

import java.io.Serializable;
import java.util.ArrayList;

import a21404080_21402048_21403888.wemeet.R;
import a21404080_21402048_21403888.wemeet.model.FriendsInfo;
import a21404080_21402048_21403888.wemeet.model.FriendsLocation;
import a21404080_21402048_21403888.wemeet.model.Room;


public class MapActivity extends FragmentActivity implements OnMapReadyCallback, LocationListener, Serializable {

    private GoogleMap mMap;
    private Marker myMarker;
    private LocationManager locationManager;
    private static final String PREFS_NAME = "MyPrefsFile";

    private FirebaseDatabase database;
    private FirebaseUser firebaseUser;
    private DatabaseReference myLat;
    private DatabaseReference myLng;
    private FriendsLocation userLoggedIn;
    private UserInfo userInfo;
    private StorageReference mStorageRef;
    private Marker meetMarker;
    private ArrayList<FriendsInfo> roomWFriends;
    private DatabaseReference myNames;
    private Room room;
    private ArrayList<Marker> markerPoints;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_layout);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        userInfo = FirebaseAuth.getInstance().getCurrentUser();
        database = FirebaseDatabase.getInstance();

        roomWFriends = new ArrayList<>();

        myLat = database.getReference("Users").child(userInfo.getDisplayName()).child("latitude");
        myLng = database.getReference("Users").child(userInfo.getDisplayName()).child("longitude");
        Log.d("TAG", "onCreate: Lat " + myLat + " Lng " + myLng);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        Bundle b = getIntent().getExtras();
        String intentAddress = b.getString("morada");
        room = (Room) b.getSerializable("room");

        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);

        LatLng locationLatLng = new LatLng(room.getLatitude(), room.getLongitude());
        //Add marker local de encontro
        meetMarker = mMap.addMarker(new MarkerOptions().position(locationLatLng).title(intentAddress).icon(BitmapDescriptorFactory
                .defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
        Log.d("TAG", "onMapReady: " + room.getLatitude() + "" + room.getLongitude());
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(locationLatLng, 10.0f));


        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){

            ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION);
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 100);
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);


        final DatabaseReference usersRef = database.getReference("Users");// ligação ao firebase, tabela users
        markerPoints = new ArrayList<>();
        for(int i = 0; i < room.getMemberList().size()-1; i++) {
            final String name = room.getMemberList().get(i).getName();
            final int a = i;
            usersRef.addValueEventListener(new ValueEventListener() { // A função atualizará sempre que algo e mudado e a iniciar //classe anónima
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    double friendsLat = (double) dataSnapshot.child(name).child("latitude").getValue();
                    double friendsLng = (double) dataSnapshot.child(name).child("longitude").getValue();
                    LatLng friendsLatLng = new LatLng(friendsLat, friendsLng);
                    if(a < markerPoints.size()) { // como o i é index começará no 0
                        markerPoints.get(a).setPosition(friendsLatLng); // atualiza markers
                    } else {
                        markerPoints.add(mMap.addMarker(new MarkerOptions().position(friendsLatLng).title(name))); // adiciona markers
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults.length>0 && permissions[0].equals(Manifest.permission.ACCESS_FINE_LOCATION)){
        }
    }

    @Override
    public void onLocationChanged(Location location) { //Called when the location has changed.
        LatLng mylatLng = new LatLng(location.getLatitude(), location.getLongitude());
        if(myMarker != null){
            myMarker.setPosition(mylatLng); // atualiza o meu marker
        } else {
            myMarker = mMap.addMarker(new MarkerOptions().position(mylatLng).title("" + userInfo.getDisplayName())); // adiciona o meu marker
        }
        Log.d("TAG", "onLocationChanged: nome " + userInfo.getDisplayName());
        //store in firebase
        myLat.setValue(location.getLatitude());
        myLng.setValue(location.getLongitude());
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {}
    @Override
    public void onProviderEnabled(String provider) {}
    @Override
    public void onProviderDisabled(String provider) {}
}
