package a21404080_21402048_21403888.wemeet.controller;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;

import a21404080_21402048_21403888.wemeet.R;
import a21404080_21402048_21403888.wemeet.model.FriendsInfo;
import a21404080_21402048_21403888.wemeet.model.Room;
import a21404080_21402048_21403888.wemeet.utils.VeryFriendsAdaptar;

public class FriendsActivity extends AppCompatActivity implements Serializable{

    private ListView listView;
    private Button buttonCancel;
    private Button buttonOk;
    private SwipeRefreshLayout swipeRefreshLayout;

    private static final int PLACE_PICKER_REQUEST = 1;

    private ArrayList<FriendsInfo> veryFriends;
    private ArrayList<FriendsInfo> roomWFriends;
    private ArrayAdapter<String> adapter;
    private FirebaseAuth firebaseAuth;
    private FirebaseUser user;
    private FirebaseDatabase database;
    private DatabaseReference friendsRef;
    private DatabaseReference roomdbref;
    private DatabaseReference membersref;



    private Room room;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.friends_activity_layout);

        database = FirebaseDatabase.getInstance();
        friendsRef = database.getReference("Users");

        friendsRef.addValueEventListener(new ValueEventListener() { //Ao ser adicionado ou removido algo da bd esta funcao atualizará o veryFriends
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Map<String, String> value = (Map<String, String>) dataSnapshot.getValue();
                veryFriends = new ArrayList<>();
                if (value == null) {
                    return;
                }
                for (String nameFromFirebase : value.keySet()) {
                    boolean found = false;
                    for (FriendsInfo friendLocal : veryFriends) {
                        if (friendLocal.getName().equals(nameFromFirebase)) {
                            found = true;
                        }
                    }
                    if (!found) {
                        veryFriends.add(new FriendsInfo(nameFromFirebase));
                    }
                }
                listView.setAdapter(new VeryFriendsAdaptar(getApplicationContext(), veryFriends));
            }


            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent, R.color.colorFb, R.color.colorGp);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(false);
                        // Irá buscar os veryFriends e atualizar a página
                        listView.setAdapter(new VeryFriendsAdaptar(getApplicationContext(), veryFriends));
                    }
                }, 3000);
            }
        });

        listView = (ListView) findViewById(R.id.friends_list);
        roomWFriends = new ArrayList<>();



        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("TAG", "onItemClick: CLICKADO");
                if(!roomWFriends.contains(veryFriends.get(position))) {
                    roomWFriends.add(veryFriends.get(position));
                    view.setBackgroundColor(getResources().getColor(R.color.colorYellow));
                    //view.getBackground().setColorFilter(Color.parseColor("#00FF00"), PorterDuff.Mode.DARKEN);
                } else {
                    roomWFriends.remove(veryFriends.get(position));
                    view.setBackgroundColor(Color.TRANSPARENT);
                }
            }
        });



        buttonCancel = (Button) findViewById(R.id.button_cancel);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });
        buttonOk = (Button) findViewById(R.id.button_ok);
        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                try {
                    Intent intent = builder.build(FriendsActivity.this);
                    startActivityForResult(intent, PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }

            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PLACE_PICKER_REQUEST && resultCode == RESULT_OK) {
            Place place = PlacePicker.getPlace(FriendsActivity.this, data);

            String address = place.getAddress().toString();
            String strLatLng = place.getLatLng().toString().replace("lat/lng: (", "");
            String newStrLatLng = strLatLng.substring(0, strLatLng.length() - 1);

            Log.d("TAG", "onActivityResult: " + place.getLatLng());
            Log.d("TAG", "onActivityResult: " + newStrLatLng);

            String[] latLng = newStrLatLng.split(",");
            double latitude = Double.parseDouble(latLng[0]);
            Log.d("TAG", "onActivityResult: latitude : " + latitude);
            double longitude = Double.parseDouble(latLng[1]);
            Log.d("TAG", "onActivityResult: longitude : " + longitude);
            Intent intent = new Intent(getApplicationContext(), MapActivity.class);
            Bundle b = new Bundle();
            b.putString("morada", address);


            room = new Room(latitude, longitude, roomWFriends);
            //Adicionar tabela Room à bd
            firebaseAuth = FirebaseAuth.getInstance();
            user = firebaseAuth.getCurrentUser();
            room.addMember(new FriendsInfo(user.getDisplayName()));
            //Neste FriendInfo com o nome do utilizador logado devo adicioná-lo ao grupo
            roomdbref = database.getReference("Rooms");

            if(roomdbref != null){
                roomdbref.removeValue();
            }
            roomdbref.child("meetingPlace").child("latitude").setValue(latitude);
            roomdbref.child("meetingPlace").child("longitude").setValue(longitude);
            if (membersref != null) {
                membersref.removeValue();
            }
            String  membros;
            membersref = database.getReference("Rooms").child("Members");
            for(int i = 0; i < room.getMemberList().size(); i++) {
                membros = room.getMemberList().get(i).getName();
                membersref.child(membros).setValue("");
            }
            b.putSerializable("room", room);

            intent.putExtras(b);
            startActivity(intent);
        }
    }
}