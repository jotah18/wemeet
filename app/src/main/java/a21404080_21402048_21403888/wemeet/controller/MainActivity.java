package a21404080_21402048_21403888.wemeet.controller;

import android.Manifest;
import android.accounts.Account;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;

import a21404080_21402048_21403888.wemeet.R;
import a21404080_21402048_21403888.wemeet.model.FriendsInfo;
import a21404080_21402048_21403888.wemeet.model.FriendsLocation;
import a21404080_21402048_21403888.wemeet.utils.FriendsRetriever;

//TODO: organizar em MVC, adaptar FriendsLocation

public class MainActivity extends AppCompatActivity implements View.OnClickListener, FriendsRetriever.AsyncResponse{

    private CheckBox dontShowAgain;
    public static final String PREFS_NAME = "MyPrefsFile1";

    private static final int MY_PERMISSION_FINE_LOCATION = 101;


    private ImageView imageViewMap;

    private GoogleSignInAccount acct;
    private GoogleSignInResult result;


    private DatabaseReference latitude;
    private DatabaseReference longitude;
    private DatabaseReference tokken;

    private FirebaseAuth firebaseAuth;
    private FirebaseUser user;

    private FirebaseDatabase firebaseDatabase;
    private FirebaseInstanceId firebaseInstanceId;

    private FriendsLocation friendLocation;

    private static final int RC_REAUTHORIZE = 101;
    private static final int RC_SIGN_IN= 102;

    private ConnectivityManager cm;
    private NetworkInfo activeNetwork;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        activeNetwork = cm.getActiveNetworkInfo();

        firebaseDatabase = FirebaseDatabase.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseInstanceId = FirebaseInstanceId.getInstance();
        user = firebaseAuth.getCurrentUser();

        checkConnection();


        latitude = firebaseDatabase.getReference("Users").child(user.getDisplayName()).child("latitude");
        longitude = firebaseDatabase.getReference("Users").child(user.getDisplayName()).child("longitude");
        tokken = firebaseDatabase.getReference("Users").child(user.getDisplayName()).child("Tokken_ID");

        latitude.setValue("");
        longitude.setValue("");
        tokken.setValue(firebaseInstanceId.getToken());


        imageViewMap = (ImageView) findViewById(R.id.imageView_get_started);
        imageViewMap.setOnClickListener(this);

        requestPermission();
    }

    private void checkConnection(){
        if (activeNetwork == null) { // connected to the internet
            firebaseAuth.signOut();
            finish();
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }else{
            dialogShow();
        }
    }

    private void dialogShow() {
        String dialogMessage = this.getResources().getString(R.string.dialog_message);
        String dialogTitle = this.getResources().getString(R.string.dialog_title);

        AlertDialog.Builder adb=new AlertDialog.Builder(MainActivity.this);
        LayoutInflater adbInflater = LayoutInflater.from(MainActivity.this);
        View eulaLayout = adbInflater.inflate(R.layout.checkbox, null);
        dontShowAgain = (CheckBox)eulaLayout.findViewById(R.id.skip);
        adb.setView(eulaLayout);
        adb.setTitle(dialogTitle);
        adb.setMessage(dialogMessage);
        adb.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                String checkBoxResult = "NOT checked";
                if (dontShowAgain.isChecked())  checkBoxResult = "checked";
                SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putString("skipMessage", checkBoxResult);
                // Commit the edits!
                editor.apply();
            } });

        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        String skipMessage = settings.getString("skipMessage", "NOT checked");
        if (!skipMessage.equals("checked") ) adb.show();
    }

    @Override
    public void onClick(View v) {
        if(v == imageViewMap){
            Intent intent = new Intent(this, FriendsActivity.class);
            startActivityForResult(intent, RC_SIGN_IN);
        }
    }



    public void retrieveFriendsListPressed() {
        if (acct != null) {
            Log.d("TAG", "retrieveFriendsListPressed: " + acct.getDisplayName());
            new FriendsRetriever(getApplicationContext(),
                    new Account(acct.getEmail(), "com.google"), (FriendsRetriever.AsyncResponse) this).execute();
        }
    }



    private void requestPermission() {
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSION_FINE_LOCATION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode){
            case MY_PERMISSION_FINE_LOCATION:
                if(grantResults[0] != PackageManager.PERMISSION_GRANTED){
                    Toast.makeText(getApplicationContext(), "This app requires location permissions to be granted", Toast.LENGTH_LONG).show();
                    finish();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            Log.d("TAG", "signin");
            result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            acct = result.getSignInAccount();
            retrieveFriendsListPressed();
        } else if (requestCode == RC_REAUTHORIZE) {
            Log.d("TAG", "resultCode2:" + resultCode);
        }


    }

    // this is called when friendsRetriever needs the user to accept permissions
    public void needsPermission(Intent intent) {
        startActivityForResult(intent, RC_REAUTHORIZE);
    }

    // this is called when friendsRetriever finished retrieving friends from server
    public void retrieveFinish(ArrayList<FriendsInfo> friends) {
        Intent intent = new Intent(getApplicationContext(), FriendsActivity.class);
        intent.putExtra("model", friends);
        startActivity(intent);
    }
}

