package a21404080_21402048_21403888.wemeet.utils;

import android.app.DialogFragment;
import android.os.Bundle;

/**
 * Created by JoãoPedro on 31/05/2017.
 */

public class IntroDialogFragment extends DialogFragment {

    static IntroDialogFragment newInstance(int num){
        IntroDialogFragment IntroDiaFrag = new IntroDialogFragment();

        Bundle args = new Bundle();
        args.putInt("num", num);
        IntroDiaFrag.setArguments(args);

        return IntroDiaFrag;


    }
}
