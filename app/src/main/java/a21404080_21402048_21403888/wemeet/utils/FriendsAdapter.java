package a21404080_21402048_21403888.wemeet.utils;

import android.content.Context;
import android.icu.text.LocaleDisplayNames;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import a21404080_21402048_21403888.wemeet.R;
import a21404080_21402048_21403888.wemeet.model.FriendsInfo;




public class FriendsAdapter extends ArrayAdapter<FriendsInfo> {
    private Context context;
    private ArrayList<FriendsInfo> friends;

    public FriendsAdapter(Context context, ArrayList<FriendsInfo> friends){
        super(context, 0, friends);
        this.context = context;
        this.friends = friends;
    }

    /*@NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        ViewHolder viewHolder;
        if(view == null){
            view = LayoutInflater.from(context).inflate(R.layout.friend_list_item, parent, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
            Log.d("TAG", "getView: called()");
        }else{
            viewHolder = (ViewHolder) view.getTag();
        }

        FriendsInfo friendsInfo = friends.get(position);
        viewHolder.friendsName.setText(friendsInfo.getName());
        Log.d("TAG", "getView: called() url: " + friendsInfo.getName());
        Picasso.with(context).load(friendsInfo.getPhotoUrl()).into(viewHolder.friendsPhoto);

        return view;
    }



    private class ViewHolder{
        private TextView friendsName;
        private ImageView friendsPhoto;
        ViewHolder(View view){
            friendsName = (TextView) view.findViewById(R.id.friends_name);
            friendsPhoto = (ImageView) view.findViewById(R.id.friends_photo);
        }
    }*/
}