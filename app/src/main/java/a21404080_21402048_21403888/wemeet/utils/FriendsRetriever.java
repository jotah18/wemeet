package a21404080_21402048_21403888.wemeet.utils;

import android.accounts.Account;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.people.v1.People;
import com.google.api.services.people.v1.PeopleScopes;
import com.google.api.services.people.v1.model.ListConnectionsResponse;
import com.google.api.services.people.v1.model.Name;
import com.google.api.services.people.v1.model.Person;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import a21404080_21402048_21403888.wemeet.model.FriendsInfo;
import a21404080_21402048_21403888.wemeet.controller.FriendsActivity;
import a21404080_21402048_21403888.wemeet.controller.MainActivity;
import a21404080_21402048_21403888.wemeet.model.FriendsInfo;



public class FriendsRetriever extends AsyncTask<Void, Void, List<Person>> {

    public interface AsyncResponse {
        void needsPermission(Intent intent);
        void retrieveFinish(ArrayList<FriendsInfo> friends);
    }

    /** Global instance of the HTTP transport. */
    private static HttpTransport HTTP_TRANSPORT = AndroidHttp.newCompatibleTransport();
    /** Global instance of the JSON factory. */
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

    private static final String TAG = "FriendsRetriever";

    private Account account;
    private Context context;
    private AsyncResponse observer;

    public FriendsRetriever(Context context, Account account, AsyncResponse observer) {
        this.context = context;
        this.account = account;
        this.observer = observer;
    }

    @Override
    protected List<Person> doInBackground(Void... params) {

        List<Person> result = null;
        try {
            GoogleAccountCredential credential =
                    GoogleAccountCredential.usingOAuth2(context,
                            Collections.singleton(PeopleScopes.CONTACTS_READONLY));
            credential.setSelectedAccount(account);
            People service = new People.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential)
                    .setApplicationName("REST API sample")
                    .build();
            ListConnectionsResponse connectionsResponse = service
                    .people()
                    .connections()
                    .list("people/me")
                    .setPageSize(200)
                    .setRequestMaskIncludeField("person.names,person.emailAddresses,person.phoneNumbers,person.photos")

                /*
                    For posterity, here is the list of valid request masks: person.addresses,
                    person.age_range, person.biographies, person.birthdays, person.bragging_rights, person.cover_photos,
                    person.email_addresses, person.events, person.genders, person.im_clients, person.interests,
                    person.locales, person.memberships, person.metadata, person.names, person.nicknames,
                    person.occupations, person.organizations, person.phone_numbers,
                    person.photos, person.relations, person.relationship_interests, person.relationship_statuses, person.residences, person.skills, person.taglines, person.urls
                */

                    .execute();
            result = connectionsResponse.getConnections();


        } catch (UserRecoverableAuthIOException userRecoverableException) {
            // Explain to the user again why you need these OAuth permissions
            // And prompt the resolution to the user again:
            Log.i(TAG, "UserRecoverableAuthIOException");
            observer.needsPermission(userRecoverableException.getIntent());
        } catch (IOException e) {
            // Other non-recoverable exceptions.
            e.printStackTrace();
        }

        return result;

    }

    @Override
    protected void onPostExecute(List<Person> connections) {
        if (connections != null && connections.size() > 0) {

            ArrayList<FriendsInfo> friends = new ArrayList<>();

            for (Person person : connections) {
                List<Name> names = person.getNames();

                if (names != null && names.size() > 0) {
                    // Log.i(TAG, "Name: " + person.getNames().get(0).getDisplayName());


                    //friends.add(new FriendsInfo(person.getNames().get(0).getDisplayName(), person.getPhotos().get(0).getUrl()));


                    Log.d(TAG, "onPostExecute: friend:" + person.getNames().get(0).getDisplayName()

                    );

                } else {
                    Log.i(TAG, "No names available for connection.");
                }

            }

            this.observer.retrieveFinish(friends);


        }
    }


}