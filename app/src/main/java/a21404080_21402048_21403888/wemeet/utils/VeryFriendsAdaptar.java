package a21404080_21402048_21403888.wemeet.utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import a21404080_21402048_21403888.wemeet.R;
import a21404080_21402048_21403888.wemeet.model.FriendsInfo;

public class VeryFriendsAdaptar extends ArrayAdapter<FriendsInfo> {
    private Context context;

    public VeryFriendsAdaptar(@NonNull Context context, ArrayList<FriendsInfo> veryFriends) {
        super(context, 0, veryFriends);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        FriendsInfo fInfo = getItem(position);
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.items_row, parent, false);
            viewHolder = new ViewHolder();
            // save the view holder on the cell view to get it back latter
            convertView.setTag(viewHolder);

            viewHolder.friendName = (TextView) convertView.findViewById(R.id.friend_name);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.friendName.setText(fInfo.getName());
        return convertView;
    }
    private class ViewHolder {
        private TextView friendName;
    }
}